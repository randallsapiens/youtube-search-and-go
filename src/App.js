/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React from 'react'
import { DefaultTheme, Provider as PaperProvider} from 'react-native-paper'
import { Provider as StoreProvider, connect} from 'react-redux'
import { PersistGate } from 'redux-persist/integration/react'
import { composeWithDevTools } from 'redux-devtools-extension'
import {createReactNavigationReduxMiddleware} from 'react-navigation-redux-helpers'
import {createStore, applyMiddleware} from 'redux'
import { persistStore } from 'redux-persist'
import createSagaMiddleware from 'redux-saga'

import paperTheme from 'app/config/paperTheme'
import rootSaga from 'app/rootSaga'
import { persistedReducer } from 'app/persist'
import NavigationApp from './navigation'
import SplashScreen from 'app/features/splashScreen/components/SplashScreen'

const sagaMiddleware = createSagaMiddleware()

const navigationMiddleware = createReactNavigationReduxMiddleware(state => state.nav);

const store = createStore(persistedReducer, composeWithDevTools(applyMiddleware(navigationMiddleware, sagaMiddleware)))

const persistor = persistStore(store);

sagaMiddleware.run(rootSaga)

const App = () => {
  return (    
    <StoreProvider store={store}>
        <PaperProvider theme={paperTheme(DefaultTheme)}>
          <PersistGate loading={<SplashScreen/>} persistor={persistor}>          
            <NavigationApp/>
          </PersistGate>
        </PaperProvider>
    </StoreProvider>
  )
}

export default App;
