import axios from 'axios'
import {YOUTUBE_API_URL, YOUTUBE_API_KEY} from './constants'

export const searchVideos = (query, maxResults = 20) => {
    return axios.get(`${YOUTUBE_API_URL}/search?part=snippet&q=${query}&maxResults=${maxResults}&type=video&key=${YOUTUBE_API_KEY}`)
}

export const fetchVideo = (id) => {
    return axios.get(`${YOUTUBE_API_URL}/videos?part=snippet,statistics&id=${id}&key=${YOUTUBE_API_KEY}`)
}

export const fetchComments = (id) => {
    return axios.get(`${YOUTUBE_API_URL}/commentThreads?part=snippet&videoId=${id}&key=${YOUTUBE_API_KEY}`)
}