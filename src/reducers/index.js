import {combineReducers} from 'redux'
import SearchVideos from 'app/features/searchVideos/reducer'
import VideoPlayer from 'app/features/videoPlayer/reducer'
import Navigation from 'app/navigation/reducer'

export default combineReducers({
    SearchVideos,
    VideoPlayer,
    nav: Navigation
})