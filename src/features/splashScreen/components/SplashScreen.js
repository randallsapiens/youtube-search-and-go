import React from 'react'
import { View } from 'react-native'
import { ActivityIndicator } from 'react-native-paper'
import styles from 'app/styles'

const SplashScreen = () => (
    <View style={styles.activityIndicatorContainer}>
        <ActivityIndicator animating={true} size="large"/>
    </View>
)


export default SplashScreen