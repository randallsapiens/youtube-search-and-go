import {SET_VIDEO_DETAILS, START_FEATCHING_DETAILS, STOP_FEATCHING_DETAILS, CLEAN_VIDEO, SET_COMMENTS } from './constants'

const initialState = {
    isFetching: false,
    video: {},
    comments: []
}

export default (state = initialState, action) => {
    switch(action.type){
        case SET_VIDEO_DETAILS: {
            return {
                ...state,
                video: action.payload
            }
        }
        case START_FEATCHING_DETAILS: {
            return {
                ...state,
                isFetching: true
            }
        }
        case STOP_FEATCHING_DETAILS: {
            return {
                ...state,
                isFetching: false
            }
        }
        case CLEAN_VIDEO: {
            return {
                ...initialState
            }
        }
        case SET_COMMENTS: {
            return {
                ...state,
                comments: action.payload
            }
        }
        default: 
            return state
    }
}