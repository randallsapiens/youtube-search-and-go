import { call, put, takeEvery, all } from 'redux-saga/effects'
import { FETCH_VIDEO_DETAILS, START_FEATCHING_DETAILS, STOP_FEATCHING_DETAILS, SET_VIDEO_DETAILS, FETCH_COMMENTS, SET_COMMENTS } from './constants'
import * as ytapi  from 'app/api/youtube'

const formatVideo = (video) => {
    return video.length > 0 ?
    {
        description: video[0].snippet.description,
        statistics: video[0].statistics
    }
    :
    {
        description: '',
        statistics: {}
    }
}

const formatComments = (comments) => { //TODO
    return comments.items.map((commentThread) => {
        return {
            username: commentThread.snippet.topLevelComment.snippet.authorDisplayName,
            comment: commentThread.snippet.topLevelComment.snippet.textDisplay,
            imageUrl: commentThread.snippet.topLevelComment.snippet.authorProfileImageUrl
        }
    })
}


function* fetchVideo(action) {
    try{
        yield put({type: START_FEATCHING_DETAILS})
        const video = yield call(ytapi.fetchVideo, action.payload)
        const formattedVideo = formatVideo(video.data.items)
        yield put({type: SET_VIDEO_DETAILS, payload: formattedVideo})
        yield put({type: STOP_FEATCHING_DETAILS})
    }catch(error){
        console.log("FETCH_FAILED", error)
    }
}

function* fetchComments(action) {
    try{
        const comments = yield call(ytapi.fetchComments, action.payload)
        const formattedComments = formatComments(comments.data)
        yield put({type: SET_COMMENTS, payload: formattedComments})
    }catch(error){
        console.log("FETCH_COMMENTS_FAILED", error)
    }
}


function* fetchVideoSaga(){
    yield takeEvery(FETCH_VIDEO_DETAILS, fetchVideo)
}

function* fetchCommentsSaga(){
    yield takeEvery(FETCH_COMMENTS, fetchComments)
}

export default function* videoPlayerSaga(){
    yield all([
        fetchVideoSaga(),
        fetchCommentsSaga()
    ])
}