import React from 'react'
import { View, ScrollView, Text } from 'react-native'
import { Appbar, ActivityIndicator } from 'react-native-paper';
import VideoPlayer from './VideoPlayer'
import VideoDetails from './VideoDetails'
import VideoComment from './VideoComment'
import styles from 'app/styles'
import {kFormatter} from 'app/utils'

class VideoPlayerScreen extends React.Component {

    constructor(props){
        super(props)

        const { id } = props.video
        props.fetchVideo(id)
        props.fetchComments(id)
    }

    static navigationOptions = (props) => ({
        header: (
            <Appbar.Header>
                <Appbar.BackAction
                    onPress={() => {props.navigation.goBack()}} 
                />
                <Appbar.Content
                    title={props.navigation.state.params ? props.navigation.state.params.title : ''}
                    subtitle={props.navigation.state.params ? props.navigation.state.params.channelTitle : ''}
                />
            </Appbar.Header>
        )
    })

    componentDidMount() {
        this.props.navigation.setParams({
            title: this.props.video.title,
            channelTitle: this.props.video.channelTitle
        })
    }

    componentWillUnmount(){
        this.props.cleanVideoDetails()
    }

    render() {
        const {video, API_KEY, isFetching, videoDetails, comments} = this.props
        return isFetching ? 
        ( 
            <View style={styles.activityIndicatorContainer}>
                <ActivityIndicator animating={true} />
            </View>
        ) : 
        (
            <View>
                <VideoPlayer id={video.id} API_KEY={API_KEY}/>
                <ScrollView>
                    <VideoDetails title={video.title} description={videoDetails.description} statistics={videoDetails.statistics} />
                    <View style={styles.rowReverseContainer}>
                        <Text style={{marginRight: 25}}>Comments({videoDetails.statistics ? kFormatter(videoDetails.statistics.viewCount) : '0'})</Text>
                    </View>
                    {
                        comments.map((comment, key) => (
                            <VideoComment key={key} username={comment.username} userImage={comment.imageUrl} comment={comment.comment} />
                        ))
                    }
                </ScrollView>
            </View>
        )
    }
}

export default VideoPlayerScreen