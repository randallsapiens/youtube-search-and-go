import React from 'react'
import YouTube from 'react-native-youtube'
import {ActivityIndicator} from 'react-native-paper'
import { View } from 'react-native'
import styles from 'app/styles'

class VideoPlayer extends React.Component {

    state = {
        ready: false
    }

    componentDidMount(){
        this.setState({ready: true})
    }
    
    render(){
        const {API_KEY, id} = this.props
        const {ready} = this.state
        return (
            ready ? 
            <View>
                <YouTube
                apiKey={API_KEY}
                videoId={id}   // The YouTube video ID
                play
                style={{ alignSelf: 'stretch', height: 250 }}
                />
            </View>
            :
            <View style={styles.activityIndicatorContainer}>
                <ActivityIndicator animating={true} />
            </View>
        )
    }
}

export default VideoPlayer