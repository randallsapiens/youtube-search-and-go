import React from 'react'
import { Avatar, Card, Paragraph } from 'react-native-paper'

const VideoComment = ({username, userImage, comment}) => (
    <Card>
        <Card.Title
            titleStyle={{fontSize: 16}}
            title={username}
            left={() => <Avatar.Image size={32} source={{uri: userImage}} />}
        />
       <Card.Content>
           <Paragraph>{ comment }</Paragraph>
       </Card.Content>
    </Card>
)

export default VideoComment