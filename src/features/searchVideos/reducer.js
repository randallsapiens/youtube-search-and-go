import {SET_RESULTS_VIDEOS, SET_SELECTED_VIDEO, START_SEARCHING, STOP_SEARCHING} from './constants'

const initialState = {
    resultVideos: [],
    selectedVideo: [],
    isSearching: false
}

export default (state = initialState, action) => {
    switch(action.type){
        case SET_RESULTS_VIDEOS: {
            return {
                ...state,
                resultVideos: action.payload
            }
        }
        case SET_SELECTED_VIDEO: {
            return {
                ...state,
                selectedVideo: action.payload
            }
        }
        case START_SEARCHING: {
            return {
                ...state,
                isSearching: true
            }
        }
        case STOP_SEARCHING: {
            return {
                ...state,
                isSearching: false
            }
        }
        default: 
            return state
    }
}