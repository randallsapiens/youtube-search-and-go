import RootNavigator from 'app/navigation/navigationStack'
import {NavigationActions} from 'react-navigation'

const initialAction = {type: NavigationActions.init}
const initialState = RootNavigator.router.getStateForAction(initialAction)

export default (state = initialState, action) => {
  return RootNavigator.router.getStateForAction(action, state)
}